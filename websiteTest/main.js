Vue.component("product", {
    props: {
        premium: {
            type: Boolean,
            required: true
        }
    },
    template: 
    `
    <div class="product">

        <div class="product-image">
            <a :href="link">

                <!--Attribute binding, 
                    v-bind:-->
                <img v-bind:src="image">
            </a>
        </div>

        <div class="product-info">
            <!--<h1>{{ brand }} {{ product }}</h1>-->
            <!--Computed property "title"-->
            <h1>{{ title }}</h1>
            <p>Website is under development: {{ development }} </p>

            <!-- Conditional rendering, 
                v-if/v-else-->
            <p v-if="inDev">Under Development</p>
            <p v-else>Project Ended</p>
            <p v-bind:src="deadline">Days to deadline: {{deadline}}</p>
            <p v-if="deadline >= 2">I have plenty of time!</p>
            <p v-else-if="deadline == 1">Deadline is approaching!</p>
            <p v-else-if="deadline == 0">Deadline missed..</p>

            <!-- v-show evaluates a boolean and displays or hides the value-->
            <p v-show="timeToDeadline">There's still time!</p>

            <!--Event Listening,
                v-on listens for an event and triggers an action-->
            <button v-on:click="incrButtonValue">Gimme time</button>
            <button @click="decrButtonValue" 
                    :disabled="!inDev"
                    :class="{ disabledButton: !inDev }">Take my time</button>
                    <!--Class Binding-->
                    <!--css class "disabledButton" is binded to inDev var and is applied when inDev is false-->

            <p>Features to add:</p>

            <ul>
                <!--List rendering, 
                    v-for iterates over the collection "components" and outputs a "component" for each iteration-->
                <li v-for="component in components">{{ component }}</li>
            </ul>

            <p>Types of Graphs to use:</p>
            <div v-for="variant in dataDisplay" 
                :key="variant.variantId"
                class="button-image"
                :style="{ backgroundImage: 'url(' + variant.imageResized + ')' }"
                @mouseover="updateImage(variant.variantImage)">
                <!--v-on: (or "@")
                :style allows to stylize the div we're working on-->
            </div>
        </div>
    </div>  
    `,

    //Properties for data
    data() {
        return {
            brand: "Human Experience",
            product: "Webpage",
            image: "./assets/logo_apprise.png",
            link: "https://www.humanexperience.pt/",
            inDev: true,
            deadline: 1,
            timeToDeadline: false,
            components: ["slider", "check-box", "date picker", "dropdown"],
            dataDisplay: [
                {
                    variantId: "graph0",
                    variantType: "line",
                    variantImage: "./assets/linechartpng.png",
                    imageResized: "./assets/linechartpngresized.jpg"
                },
                {
                    variantId: "graph1",
                    variantType: "bar",
                    variantImage: "./assets/barchart.png",
                    imageResized: "./assets/barchartresized.jpg"
                }
            ],
        }
    }, 

    //Properties for methods
    methods: {
        incrButtonValue: function () {
            this.deadline += 1
            if(this.deadline >= 1) {
                this.inDev = true
            }
        },
        decrButtonValue() {
            this.deadline -= 1
            if(this.deadline == 0) {
                this.inDev = false
            }
        },
        updateImage: function (variantImage) {
            this.image = variantImage
        }
    },

    //Computed properties
    computed: {
        title() {
            return this.brand +' ' + this.product
        },
        development() {
            if (this.premium) {
                return "Yes"
            }
            return "No"
        }
    }
})


var app = new Vue({
    el: '#app',
    //"data" is passed to the component as a "prop"     
    data: {
        premium: true
    }
})  