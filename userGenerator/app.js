const app = Vue.createApp({
  data() {
    return {
      firstName: "John",
      lastName: "Doe",
      gender: "male",
      phoneNumber: "123256789",
      email: "bla@bla.com",
      picture: "https://randomuser.me/api/portraits/men/10.jpg",
    };
  },

  methods: {
      assync getUser() {
          const res = await fetch('https://randomuser.me/api')
          const data = await res.json
      }
  }

});




app.mount("#app");
