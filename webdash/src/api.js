var data = {
    "device":"7c9ebdd991ac",
    "nickname":"Test1",
    "type":"alert",
    "subtype":"result",
    "timestamp":1616752023,
    "payload": {
        "wifi_report": {
            "ssid":"Globaltronic",
            "rssi":-55,
            "ssid_alt":"Hugo_wifi",
            "rssi_alt":-120,
            "connected":"Globaltronic"
        },
        "ccs811": {
            "eCO2":1573,
            "tVOC":204,
            "eCO2_offset":0,
            "tVOC_offset":0,
            "timestamp":86694
        },
        "opt3001": {
            "lux":660,
            "lux_offset":0,
            "timestamp":86696
        },
        "tcs3472": {
            "red":660,
            "blue":561,
            "green":636,
            "clear":1989,
            "light_temp_raw":58053,
            "light_temp_off":58053,
            "red_offset":0,
            "blue_offset":0,
            "green_offset":0,
            "clear_offset":0,
            "timestamp":86698 
        },
        "veml3328": {
            "R":676,
            "G":1197,
            "B":594,
            "C":2494,
            "IR":20,
            "R_offset":0,
            "G_offset":0,
            "B_offset":0,
            "C_offset":0,
            "IR_offset":0,
            "timestamp":86701
        },
        "stts751": {
            "temperature":187,
            "temperature_offset":0,
            "timestamp":86697 
        },
        "si7021": {
            "temp":188,
            "humidity":480,
            "temp_offset":0,
            "humidity_offset":0,
            "timestamp":86702
        },
        "ics43434": {
            "DBmax":608,
            "DBavg":489,
            "DBmin":426,
            "DBmax_offset":0,
            "DBavg_offset":0,
            "DBmin_offset":0,
            "timestamp":86693
        }   
    }    
}

export default {
    serveData: function() {
        return data;
    }
}