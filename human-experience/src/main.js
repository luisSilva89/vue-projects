// importing creatApp method from vue
import { createApp } from 'vue'

// importing the root "App" component from base directory
import App from './App.vue'

// creat the app passing the main 'App' component and mounting it on the div with id 'app' (at index.html)
createApp(App).mount('#app')
