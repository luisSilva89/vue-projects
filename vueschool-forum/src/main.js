import { createApp } from 'vue'
import App from './App.vue'
import router from '@/router/index'
import AppDate from '@/components/AppDate'

const forumApp = createApp(App)
forumApp.use(router)
// the base component AppDate (App word is used as a generic word to identify base components)
// is injected on our app globaly so we don´t have to import and declare as base component in all components
// we may use it
forumApp.component('AppDate', AppDate)
forumApp.mount('#app')
